def pig_it(text):
    return ' '.join([convert_string(s) for s in text.split()])

def convert_string(s):
    if s.isalpha():
        return s[1:len(s)] + s[0:1] + 'ay'
    else:
        return s

print(pig_it('Pig latin is cool'))
print('??? igPay atinlay siay oolcay')

print(pig_it('This is my string'))
print('??? hisTay siay ymay tringsay')

print(pig_it('Hello world !'))
print('elloHay orldway !')